package main

import (
	"os"

	"gitlab.com/rusher/example_go_cli/cmd"
)

func main() {
	cmd.Execute(os.Args)
}
