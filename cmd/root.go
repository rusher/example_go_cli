package cmd

import (
	log "github.com/sirupsen/logrus"

	"github.com/urfave/cli"
)

// Execute runs the cmd app
func Execute(args []string) {
	app := cli.NewApp()

	app.Version = Version

	app.Commands = []cli.Command{
		helloCommand,
	}

	err := app.Run(args)
	if err != nil {
		log.Fatal(err)
	}
}
