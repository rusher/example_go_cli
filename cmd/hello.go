package cmd

import (
	"fmt"

	"github.com/urfave/cli"
)

var helloCommand cli.Command

func init() {
	var full bool

	helloCommand = cli.Command{
		Name:  "hello",
		Usage: "say hello",
	}
	helloCommand.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "full",
			Usage:       "full name",
			Destination: &full,
		},
	}
	helloCommand.Action = func(c *cli.Context) error {
		args := c.Args()
		if len(args) < 1 {
			return fmt.Errorf("No args")
		}
		if len(args) == 1 || !full {
			fmt.Printf("Hello, %s\n", args[0])
		} else {
			fmt.Printf("Hello, %s %s\n", args[0], args[1])
		}
		return nil
	}
}
