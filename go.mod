module gitlab.com/rusher/example_go_cli

go 1.12

require (
	github.com/sirupsen/logrus v1.4.1
	github.com/urfave/cli v1.20.0
)
